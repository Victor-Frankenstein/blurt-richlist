require('dotenv').config()
const blurt = require('@blurtfoundation/blurtjs');
const _ = require('lodash');
const moment = require('moment');
const mongoose = require('mongoose');
const CronJob = require('cron').CronJob;

const RPC_URL = process.env.RPC_URL || 'https://rpc.blurt.world';
const BLOCK_GET_INTERVAL = parseInt(process.env.BLOCK_GET_INTERVAL || 2500);
const GLOBAL_GET_INTERVAL = parseInt(process.env.GLOBAL_GET_INTERVAL || 3000);
const EVERY_MINUTE = "0 * * * * *";
const EVERY_HOUR = "0 0 * * * *";

blurt.api.setOptions({ url: RPC_URL, useAppbaseApi: true });

var db1 = mongoose.createConnection(process.env.MONGODB_URI, { useNewUrlParser: true });
const { AccountSchema } = require('./src/schemas');
const Account = db1.model('Account', AccountSchema);

const job = new CronJob(EVERY_MINUTE, function() {
});
job.start();
db1.then(async (err, result) => {
  get_all_accounts();
  streamChain();
});

//This method will start on bot ready...
function streamChain() {
  let head_block = 0;
  let last_block = 0;

  setInterval(function () {
    blurt.api.getDynamicGlobalProperties(function (err, result) {
      if (!_.isEmpty(result)) head_block = result.head_block_number;
    });
  }, GLOBAL_GET_INTERVAL);

  setInterval(function () {
    if (head_block > last_block) {
      if (last_block === 0) {
        last_block = head_block;
      }
      processBlock(last_block)
      last_block = last_block + 1;
    }
  }, BLOCK_GET_INTERVAL);

} // Send activity function end.

function processBlock(current_block) {
  blurt.api.getOpsInBlock(current_block, false, (err, result) => {
    if (err) {
      console.log('get block error', err);
      return;
    }

    // process un-empty blocks only...
    if (!_.isEmpty(result)) {
      // update account for any transactions that increases an account's value
      let process_txs = [
        'author_reward', 'comment_benefactor_reward', 'comment_reward', 'curation_reward',
        'fill_convert_request', 'fill_order', 'fill_transfer_from_savings', 'fill_vesting_withdraw',
        'liquidity_reward', 'producer_reward', 'proposal_pay', 'return_vesting_delegation',
        'sps_fund'
      ];
      result.forEach(tx => {
        let txType = tx.op[0];
        let txData = tx.op[1];
        if (process_txs.indexOf(txType) >= 0) {
          let account_name = "";
          if (txType == "author_reward") {
            account_name = txData
          }

          if (!_.isEmpty(account_name)) {
            updateAccountInfo(account_name)
          }
        }
      });
    }
  });
}

async function updateAccountInfo(account) {
  blurt.api.getAccounts([account], function (err, result) {
    if (err) return;
    let account_info = result[0];
    console.log('account', account)
    let item = Account.findOne({ name: account });
    _.extend(item, {
      name: account,
      ...account_info
    });
    item.save().catch(error => {
      if (error.name === 'MongoError' && error.code === 11000) {
        console.log('duplicate error: ', account)
      }
    });
  });
}

async function get_all_accounts(start='', stop='', steps=1e3, limit=-1) {
  let cnt = 1;
  let lastname = ""
  if (start !== "") {
    lastname = start
  }

  try {
    while (true) {
      await new Promise(r => setTimeout(r, 200));
      let ret = await blurt.api.lookupAccountsAsync(lastname, steps);
      console.log('ret', ret)

      let account_name = "";
      for (let i = 0; i < ret.length; i++) {
        let account = ret[i];
        blurt.api.getAccounts([account], function (err, result) {
          if (err) return;
          let account_info = result[0];
          console.log('account', account)
          let item = new Account();
          _.extend(item, {
            name: account,
            ...account_info
          });
          item.save().catch(error => {
            if (error.name === 'MongoError' && error.code === 11000) {
              console.log('duplicate error: ', account)
            }
          });
        });

        account_name = account;
        if (account_name != lastname) {
          cnt += 1
          if (account_name == stop || (limit > 0 && cnt > limit)) {
            return
          }
        }
      }
      console.log('lastname == account_name', lastname == account_name)
      if (lastname == account_name) {
        return
      }

      lastname = account_name
      console.log('ret.length < steps', ret.length < steps)
      if (ret.length < steps) {
        return
      }
    }
  } catch (err) {
    console.log('err', err)
  }

}