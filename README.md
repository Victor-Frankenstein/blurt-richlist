## How to use it

Create a `.env` and place MongoDB URL to store accoutns.

```
MONGODB_URI=mongodb://localhost:27017/blurt_richlist
RPC_URL=https://rpc.blurt.world
```

Starting the server for development `npm run dev`

For production, I would suggest using PM2 Library. (Install with `npm install -g pm2`)

Then, run `pm2 start index.js`
